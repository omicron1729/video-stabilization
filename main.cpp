#include <QtGui/QApplication>
#include "mainwindow.h"
#include "opencv2/opencv.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/core/core.hpp"
#include "opencv2/features2d/features2d.hpp"
#include <stdio.h>
#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/nonfree/nonfree.hpp"
#include <iostream>


using namespace cv;

Mat Ai;
Mat Acum=Mat::eye(3,3,CV_32FC1);

int main(int argc, char *argv[])

{

    QApplication a(argc, argv);
    MainWindow w;

    //w.show();

    VideoCapture cap("/home/manas/Desktop/i4 Quadcopter, homemade wood frame with Naza-M + GPS, DragonLink.mp4");

    namedWindow("video",1);

    Mat vid[30];

    int i=0;

    std::vector<KeyPoint> keypoints_f1, keypoints_f2;


    for(;;)

    {
        if(i>=29)
        {
            i=0;
        }

        cap>>vid[i];

        cvtColor(vid[i],vid[i],CV_BGR2GRAY);

        if(i>0)
        {
            FASTX(vid[i-1],keypoints_f1,60,true,FastFeatureDetector::TYPE_7_12);
            KeyPointsFilter::retainBest(keypoints_f1,30);

            FASTX(vid[i],keypoints_f2,60,true,FastFeatureDetector::TYPE_7_12);
             KeyPointsFilter::retainBest(keypoints_f2,30);

            SurfDescriptorExtractor extractor;
            //FREAK extractor;

            Mat descriptors_f1, descriptors_f2;

            extractor.compute( vid[i-1], keypoints_f1, descriptors_f1 );
            extractor.compute( vid[i], keypoints_f2, descriptors_f2 );

            FlannBasedMatcher matcher;

            std::vector< DMatch > matches;

            try
            {
             matcher.match( descriptors_f1, descriptors_f2, matches );
            }

            catch(Exception e)
            {
                continue;
            }

            {
              double max_dist = 0; double min_dist = 10;

              //-- Quick calculation of max and min distances between keypoints

              for( int j = 0; j < descriptors_f1.rows; j++ )
              { double dist = matches[j].distance;

                if( dist < min_dist ) min_dist = dist;
                if( dist > max_dist ) max_dist = dist;

              }


              std::vector< DMatch > good_matches;

              for( int k = 0; k< descriptors_f1.rows; k++ )
              { if( matches[k].distance < 3*min_dist )
                 { good_matches.push_back( matches[k]); }
              }


              std::vector<Point2f> obj;
              std::vector<Point2f> scene;

              for( int l = 0; l < good_matches.size(); l++ )
              {

                //-- Get the keypoints from the good matches

                obj.push_back( keypoints_f1[ good_matches[l].queryIdx ].pt );
                scene.push_back( keypoints_f2[ good_matches[l].trainIdx ].pt );
              }


              if(obj.size()>=4 && scene.size()>=4)
              {

               //   printf("%d    %d\n",obj.size(),scene.size());

              Point2f srcTri[4];
              Point2f dstTri[4];

              srcTri[0]=obj[0];
              srcTri[1]=obj[1];
              srcTri[2]=obj[2];
              srcTri[3]=obj[3];

              dstTri[0]=scene[0];
              dstTri[1]=scene[1];
              dstTri[2]=scene[2];
              dstTri[3]=scene[4];

              Mat warp_mat( 2, 3, CV_32FC1);
              Mat warp_tmp(3,3,CV_32FC1);

              warp_mat=getAffineTransform(srcTri,dstTri);

              for(int h=0;h<warp_mat.rows;h++)
              {
                  for(int q=0;q<warp_mat.cols;q++)
                  {
                      warp_tmp.at<float>(h,q)=warp_mat.at<float>(h,q);
                  }
              }

              warp_tmp.at<float>(2,0)=0;
              warp_tmp.at<float>(2,1)=0;
              warp_tmp.at<float>(2,2)=1;

              Acum=Acum*warp_tmp;

              }

        }
    }


        imshow("video",vid[i]);
        i++;

        if (waitKey(30)==27) break;
    }


    //return a.exec();

}
